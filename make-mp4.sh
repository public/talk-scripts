V_BIT_RATE=768kb
A_BIT_RATE=128kb

LOG="timings/$2-mp4.log"
TIC=`date +%s`

ffmpeg -i $1 -v -1 -y\
       -pass 1 -threads 0 -y \
       -vcodec libx264 -b "$V_BIT_RATE"  -bt "$V_BIT_RATE" \
       -vpre fastfirstpass \
       "encodes/$2-first.mp4" 2> /dev/null

ffmpeg -i $1 -v -1 -y \
       -pass 2 -threads 0 \
       -vcodec libx264 -b "$V_BIT_RATE" -bt "$V_BIT_RATE" \
       -vpre hq \
       -acodec libfaac -ac 1 -ab "$A_BIT_RATE" \
       "encodes/$2.mp4" 2> /dev/null

mv "ffmpeg2pass-0.log" "$2-ffmpeg2pass-0.log"
mv "x264_2pass.log" "$2-x264_2pass.log"

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG


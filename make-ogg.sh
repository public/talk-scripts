V_BIT_RATE=768kb
A_BIT_RATE=128kb

LOG="timings/$2-mkv.log"
TIC=`date +%s`

ffmpeg -i "$1" -y -v -1 \
       -f ogg \
       -b "$V_BIT_RATE" \
       -ac 1 -ab "$A_BIT_RATE" -aq 60 \
       "encodes/$2.ogg" 2> /dev/null

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG

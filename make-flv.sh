V_BIT_RATE=768kb
A_BIT_RATE=128kb

LOG="timings/$2-flv.log"
TIC=`date +%s`

ffmpeg -i "$1" -v -1 -y \
       -f flv -b "$V_BIT_RATE" \
       -ac 1 -ab "$A_BIT_RATE" -ar 22050 \
       "encodes/$2.flv" 2> /dev/null

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG

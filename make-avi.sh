V_BIT_RATE=768kb
A_BIT_RATE=128kb

LOG="timings/$2-avi.log"
TIC=`date +%s`

ffmpeg -i $1 -v -1 -y \
       -vcodec libxvid -b "$V_BIT_RATE" \
       -acodec libmp3lame -ac 1 -ab "$A_BIT_RATE" \
       "encodes/$2.avi" 2> /dev/null

TOC=`date +%s`

echo "`expr $TOC - $TIC`" >> $LOG

